import request from '@/utils/request'

export function generateNoAsync(prefix) {
  return request({
    url: '/api/services/app/NoAuto/GenerateNoAsync',
    method: 'post',
    params: { 'prefix': prefix }
  })
}
