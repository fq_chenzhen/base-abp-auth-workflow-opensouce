/** When your routing table is too long, you can split it into small modules**/

import Layout from '@/layout'

const flowCenterRouter = {
  path: '/flow',
  component: Layout,
  redirect: 'noRedirect',
  alwaysShow: true, // will always show the root menu
  name: 'flow',
  meta: {
    title: '流程中心',
    icon: 'tree',
    permission: 'Pages.FlowCenter'
  },
  children: [
    {
      path: 'leave-req-form',
      component: () => import('@/views/flow/leave-req-form/index'), // Parent router-view
      name: 'leave-req-form-flow',
      meta: { title: 'LeaveReqForm', permission: 'Pages.LeaveReqForm' },
      children: [
        {
          path: 'mine',
          name: 'leave-req-form-flow-mine',
          component: () => import('@/views/flow/leave-req-form/mine/index'),
          meta: {
            title: '我的请假'
            // permission: 'Pages.Administration.OrganizationUnits'
          }
        },
        {
          path: 'wait',
          name: 'leave-req-form-flow-wait',
          component: () => import('@/views/flow/leave-req-form/wait/index'),
          meta: { title: '待办请假' }
        },
        {
          path: 'disposed',
          name: 'leave-req-form-flow-disposed',
          component: () => import('@/views/flow/leave-req-form/disposed/index'),
          meta: { title: '已办请假' }
        }
      ]
    }]
}

export default flowCenterRouter
