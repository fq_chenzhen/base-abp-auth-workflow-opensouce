import request from '@/utils/request'

const state = {
  entityName: '',
  loading: false, // 控制非dialog界面的loading
  list: [],
  totalCount: 0,
  pageSize: 10,
  currentPage: 1
}

const mutations = {
  SET_ENTITY_NAME: (state, entityName) => {
    state.entityName = entityName
  },
  SET_PAGE_SIZE(state, size) {
    state.pageSize = size
  },
  SET_CURRENT_PAGE(state, page) {
    state.currentPage = page
  }
}

const actions = {
  get({ state }, payload) {
    return new Promise((resolve, reject) => {
      state.loading = true
      request({
        url: `/api/services/app/${state.entityName}/Get`,
        method: `get`,
        params: { id: payload.data.id }
      })
        .then(rep => {
          // state.currentUser = {}
          // 下面的items把对象数组展开传入，这里只传入一个
          // state.currentUser = rep.data.result
          resolve(rep.data.result)
        })
        .catch(error => {
          reject(error)
        })
        .finally(() => {
          state.loading = false
        })
    })
  },
  getAll({ state }, payload) {
    return new Promise((resolve, reject) => {
      state.loading = true
      request({
        url: `/api/services/app/${state.entityName}/GetAll`,
        method: `get`,
        params: payload.data
      })
        .then(response => {
          state.list = []
          state.list.push(...response.data.result.items)
          state.totalCount = response.data.result.totalCount
          resolve(response.data.result)
        })
        .catch(error => {
          reject(error)
        })
        .finally(() => {
          state.loading = false
        })
    })
  },
  deleteItem({ state }, payload) {
    return new Promise((resolve, reject) => {
      state.loading = true
      request({
        url: `/api/services/app/${state.entityName}/Delete`,
        method: `delete`,
        params: { id: payload.data.id }
      })
        .then(response => {
          resolve(response.data.result)
        })
        .catch(error => {
          reject(error)
        })
        .finally(() => {
          state.loading = false
        })
    })
  },
  create({ state }, payload) {
    return new Promise((resolve, reject) => {
      state.loading = true
      request({
        url: `/api/services/app/${state.entityName}/Create`,
        method: `post`,
        data: payload.data
      })
        .then(response => {
          resolve()
        })
        .catch(error => {
          reject(error)
        })
        .finally(() => {
          state.loading = false
        })
    })
  },
  update({ state }, payload) {
    return new Promise((resolve, reject) => {
      state.loading = true
      request({
        url: `/api/services/app/${state.entityName}/Update`,
        method: `put`,
        data: payload.data
      })
        .then(response => {
          resolve()
        })
        .catch(error => {
          reject(error)
        })
        .finally(() => {
          state.loading = false
        })
    })
  }
}

export default { state, mutations, actions }
