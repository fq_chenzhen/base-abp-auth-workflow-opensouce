var config = {
  authority: process.env.VUE_APP_OIDC_CONFIG_AUTHORITY,
  client_id: 'js',
  redirect_uri: `${
    process.env.VUE_APP_OIDC_CONFIG_BASE_REDIRECECT_URL
  }/#/signin-callback-oidc`,
  response_type: 'code',
  scope: 'openid profile api1 email',
  post_logout_redirect_uri: `${
    process.env.VUE_APP_OIDC_CONFIG_BASE_REDIRECECT_URL
  }/#/signout-callback-oidc`
}

export default config
